// Javascript Statements:
	/*
		>> set of instructions that we tell the computer/ machine to perform
		>> JS statements usually ends with semicolon(;), to locate where the statements end
		>> A syntax in programming, it is the set of rules that describes how statements must be constructed
		>> All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner

	*/
console.log("Hello once more");

// Comments
//- one-line comment (ctrl + /) 
/* multi-line comment (ctrl + shift + /) */
/*
	>> Comments are parts of the code that gets ignored by the language
	>> Comments are meant to describe the written code
*/

// Variables
	// used for containing data
	// Any information that is used by an application is stored in what we call a "memory"
	// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
	// This makes it easier for us associate information stored in our devices to actual "names" about information

/*
	Syntax:
		let / const variableName;
*/

// Declaring Variables
	// Declaring variables - tells our devices that a variable name is created and is ready to store data
	// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".
 let hello;
 console.log(hello); // result: undefined

// Trying to print out a value of a variable that has not been declared will return an error of "not defined"
 // console.log(x); //result: err
 // let x;

// Declaration Variables
	//Best Practices:
	/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/
let firstName = "Jin"; //good variable name
let pokemon = 25000; // bad variable name

let FirstName = "Izuku"; // bad variable name
let firstNameAgain = "All Might"; // good variable name

// let first name = "Midoriya"; // bad variable name

//camelcase
	//lastName, emailAddress. mobileNumber
// underscores
let product_description = "lorem ipsum";
let product_id = "250000ea1000";

/*
	Syntax:
		let / const variableName = value;
*/
// Initializing variables - the instance when a variable is given it's initial/starting value
let productName = "desktop computer";
console.log(productName); // result: desktop computer

let productPrice = 18999;
console.log(productPrice); //result: 18999

const pi = 3.14;
console.log(pi); //result: 3.14;

// Reassigning variable
/*
	Syntax:
		variableName = value;
*/
// Reassigning a variable means changing it's initial or previous value into another value
productName = "Laptop";
console.log(productName); // result: Laptop

let friend = "Kate";
friend = "Chance";
console.log(friend); // result: Chance

// let friend = "Jane";
// console.log(friend); // result: err "identifier friend has already been declared"

// pi = 3.1;
// console.log(pi); // result: err "assignment to constant variable"

// Reassigning vs. Initializing variable

// Initialization is done after the variable has been declared
// This is considered as initialization because it is the first time that a value has been assigned to a variable
let supplier;
supplier = "Jane Smith Tradings"
console.log(supplier); // result: Jane Smith Tradings

// reassignment
// This is considered as reassignment because it's initial value was already declared
supplier = "Zuitt Store";
console.log(supplier); // result: Zuitt Store

// var vs. let/ const
//var - is also used in declaring a variable. but var is an ECMAScript1 (ES1) feature[ES1 (JavaScript 1997)]. 
//let/const was introduced as a new feature in ES6(2015)

//There are issues associated with variables declared with var, regarding hoisting.
//Hoisting is JavaScript's default behavior of moving declarations to the top.
//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.
a = 5;
console.log(a); // result: 5
var a;

// b = 6;
// console.log(b); // result: err
// let b;

// let/ const local/ global scope
//Scope essentially means where these variables are available for use
//let and const are block scoped
//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.
//So a variable declared in a block with let  is only available for use within that block. 
let outerVariable = 'hello';

{
	let innervariable = 'hello again';
}

console.log(outerVariable); // result: hello
// console.log(innervariable); // result: innerVariable is not defined

// Multiple Variable Declaration
// Though it is quicker to do without having to retype the "let" keyword, it is still best practice to use multiple "let"/"const" keywords when declaring variables
// Using multiple keywords makes code easier to read and determine what kind of variable has been created
let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand); // result: DC017, Dell

// let is a reserved keyword
// const let = 'hello';
// console.log(let); //result: err

// Data Types
// Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote
let country = 'Philippines';
let city = "Manila City";

console.log (city, country); // result: Manila City Philippines

// Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = city + ', ' + country;
console.log(fullAddress); // result: Manila City, Philippines

/*
	Mini-Activity: 5 mins.
	1. initialize a variable called myName with your own name as a value
	2. Concatenate it with the phrase 'Hi I am'
	3. Store the concatenated strings in a variable called greeting
	4. Print out greeting in your console
	5. Send output in Hangouts
*/

// S O L U T I O N:
let myName = "Tine";
let greeting = 'Hi I am ' + myName;
console.log(greeting);

// escape character (\)
// The escape character (\) in strings in combination with other characters can produce different effects

	// "\n"- creates a new line in between text

let mailAddress = 'Metro Manila\n\nPhilippines'
console.log(mailAddress); //result line break between MM and PH

let message = 'John\'s employees went home early';
console.log(message); //prints out the message

// Numbers
// Integers/ Whole Numbers
let headcount = 26;
console.log(headcount); // result: 26

// Decimal Numbers
let grade = 74.9;
console.log(grade); // result: 74.9

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance); // result: 20000000000

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// boolean
let isSingle = true;
let isMarried = false;
console.log("isSingle " + isSingle);
console.log("isMarried " + isMarried);

// similar data types
let grades = [98.7, 77.3, 90.8, 88.4];
console.log(grades);

const anime = ["BNHA", "AOT", "SxF", "KNY"];
console.log(anime);

// different data types
let random = ["JK", 24, true];
console.log(random);

// object data type
/*
	syntax:
		let/const objectName = {
	PropertyA : valueA,
	PropertyB : valueB
		}
*/

let person = {
	fullName: "Edward Scissorhands",
	age: 35,
	isMarried: false,
	contact: ["09123456789", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
};
console.log(person)

const myGrades = {
	firstGrading: 98.7,
	secondGrading: 77.3,
	thirdGraing: 88.6
};
console.log(myGrades);

// typeof operator
console.log(typeof myGrades); 
console.log(typeof grades);
console.log(typeof grade);

// anime = ['One Punch Man'];
// console.log(anime);

anime[0]= ['One Punch Man'];
console.log(anime);

// Null
let spouse = null;
console.log(spouse);

let zero = 0;
let emptyString= '';

// undefined
let y;
console.log(y);
